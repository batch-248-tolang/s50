// Use Reacts Context API to give the authenticated user object to have a "global" scope within our application
import React from 'react';

// Creation of context object
// A context object is a data type of an object that can be used to store information that can be shared to other components within the app
// Context object is a different approach to passing info between components and allows easier access by avoiding the use of prop-drilling

const UserContext = React.createContext();
/*
	A context object is created using the React.createContext() method, which returns an object with 2 properties:
	1. Provider - is used to wrap the components that need access to the context, and it provided the context value to all of its descendants
	2. Consumer - is used to access the context value within a component
*/

// The provider component allows other components to consume/use the context object and supply the necessary information needed
export const UserProvider = UserContext.Provider;

export default UserContext;